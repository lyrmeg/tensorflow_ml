# wide and deep model with tensorflow estimator api
import shutil
import numpy as np
import tensorflow as tf

CSV_COLUMNS = 'x1, x2, x3, y'
LABEL_COLUMN = 'y'
KEY_COLUMN = 'key'

# set default values for each csv column
DEFAULTS = [[0.0], ['null'], ['nokey'], [0.0]]
TRAIN_STEPS = 1000


def read_dataset(filename, mode, batch_size=512):
    def _input_fn():
        def decode_csv(value_column):
            columns = tf.decode_csv(value_column, record_defaults=DEFAULTS)
            features = dict(zip(CSV_COLUMNS, columns))
            label = features.pop(LABEL_COLUMN)
            return features, label

        # create list of files that match pattern
        file_list = tf.gfile.Glob(filename)

        # create dataset from file list
        dataset = (tf.data.TextLineDataset(file_list).map(decode_csv))

        if mode == tf.estimator.ModeKeys.TRAIN:
            num_epochs = None  # indefinitely
            dataset = dataset.shuffle(buffer_size=10*batch_size)
        else:
            num_epochs = 1

        dataset = dataset.repeat(num_epochs).batch(batch_size)
        return dataset
    return _input_fn


def get_categorical(name, values):
    return tf.feature_column.indicator_column(
        tf.feature_column
        .categorical_column_with_vocabulary_list(name, values))


def get_cols():
    return [
        get_categorical('x2', ['a', 'b', 'null']),
        tf.feature_column.numeric_column('x1'),
        get_categorical('x3', ['a', 'b', 'nokey'])
    ]


def get_wide_and_deep():
    # define column types
    x1, x2, x3 = [
        tf.feature_column
        .categorical_column_with_vocabulary_list('x2', ['a', 'b', 'null']),
        tf.feature_column.numeric_column('x1'),
        tf.feature_column
        .categorical_column_with_vocabulary_list('x3', ['a', 'b', 'nokey'])
    ]
    # discretize
    x1_buckets = tf.feature_column.bucketized_column(x1, boundaries=np
                                                     .range(0, 5, 10)
                                                     .tolist())
    wide = [x1_buckets, x2, x3]
    # features cross all the wide columns and embed into a lower dimension
    crossed = tf.feature_column.crossed_column(wide, hash_bucket_size=2000)
    embed = tf.feature_column.embedding_column(crossed)

    deep = [x1, embed]
    return wide, deep
    # create serving input function to be able to serve predictions later using
    # provided inputs


def serving_input_fn():
    feature_placeholders = {
        'x2': tf.compat.v1.placeholder(tf.string, [None]),
        'x1': tf.compat.v1.placeholder(tf.float32, [None]),
        'x3': tf.compat.v1.placeholder(tf.string, [None])
    }
    features = {
        key: tf.expand_dims(tensor, -1)
        for key, tensor in feature_placeholders.items()
    }
    return tf.estimator.export.ServingInputReceiver(features,
                                                    feature_placeholders)

# create estimator to train and evaluate


def train_and_evaluate(output_dir):
    wide, deep = get_wide_and_deep()
    EVAL_INTERVAL = 300
    run_config = tf.estimator.RunConfig(save_checkpoints_secs=EVAL_INTERVAL,
                                        keep_checkpoint_max=3)
    estimator = tf.estimator.DNNLinearCombinedRegressor(
        model_dir=output_dir,
        linear_feature_columns=wide,
        dnn_feature_columns=deep,
        hidden_units=[64, 32],
        config=run_config
    )
    train_spec = tf.estimator.TrainSpec(
        input_fn=read_dataset('train.csv',
                              mode=tf.estimator.ModeKeys.TRAIN),
        max_steps=TRAIN_STEPS
    )
    exporter = tf.estimator.LatestExporter('exporter', serving_input_fn)
    eval_spec = tf.estimator.EvalSpec(
        input_fn=read_dataset('eval.csv',
                              mode=tf.estimator.ModeKeys.EVAL),
        steps=None,
        start_delay_secs=60,
        throttle_secs=EVAL_INTERVAL,
        exporters=exporter
    )
    tf.estimator.train_and_evaluate(estimator, train_spec, eval_spec)


shutil.rmtree('result_folder', ignore_errors=True)
tf.compat.v1.summary.FileWriteCache.clear()
train_and_evaluate('result_folder')
